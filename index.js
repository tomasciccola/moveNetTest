const http = require('http')
const fs = require('fs')
const path = require('path')
const { Server } = require("socket.io")
const { Client } = require('node-osc')
const client = new Client('127.0.0.1', 8000);

const PORT = process.env.PORT || 5000
const ROOT = 'public'
const SIZE = {x:640.0,y:480.0}

const N_POSES = 2
const PARTS = ['left_wrist','right_wrist', 'nose']


const map = (val, in_min, in_max, out_min, out_max) =>  {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
}

const onRequest = (req,res) =>{
  console.log(req.method, req.url)

  if(req.url === '/'){
    return res.end(fs.readFileSync(path.join(ROOT, 'index.html')))
  }

  if(req.url === '/index.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'index.js')))
  }

  if(req.url === '/lib/tfjs-core.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/tfjs-core.js')))
  }

  if(req.url === '/lib/tfjs-converter.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/tfjs-converter.js')))
  }

  if(req.url === '/lib/tfjs-backend-webgl.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/tfjs-backend-webgl.js')))
  }

  if(req.url === '/lib/pose-detection.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/pose-detection.js')))
  }

}

const sendData = (str) => (data) => {

  //map data
  data.x = map(data.x / SIZE.x, 0.0,1.0,1.0,0.0)
  data.y = data.y / SIZE.y 

  const dString = `/${str}/play/xy`
  console.log('on', str, 'sending!', dString, data.x, data.y)
  client.send(dString,data.x,data.y)
}

// MAIN
const server = http.createServer(onRequest)
const io = new Server(server);

io.on('connection', socket => {
  console.log('conn!')

  for(let i = 0; i < N_POSES; i++){
    for(let j = 0; j < PARTS.length; j++){
      const str = `pose_${i}_${PARTS[j]}`
      socket.on(str, sendData(str))
    }
  }
})

server.listen(PORT, () => console.log(`servidor esuchando en ${PORT}`))
