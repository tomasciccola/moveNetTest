# moveNet test
experimentos con modelos de detección de cuerpos en tiempo real, usando ts.js.

Como modelo default se usa moveNet. El tema con moveNet es que no te da profundidad (keypoints3d), 
que sí te da BlazeNet, pero no pude hacer que me devuelva nada.

Para correr por ahora es una web. Estaría bueno después correrlo headless desde node, 
pero levantar un stream de la webcam desde node en modo headless no estaría siendo 
muy trivial (contra todo pronóstico...)


```
npm start # y ya
```

Por ahora todo el código está en public.
el index.js que está en el ROOT está para servir la web, 
y después para pruebas en node directamente

## TODO
* correr en node
* mandar osc 
  * filtrar la señal para que no sea mil manija
* si no puedo correr en node, hacer un bridge websocket -> node-osc -> audiostellar
