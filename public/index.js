const EAGER = true

const video = document.querySelector('video')
const canvas = document.querySelector('canvas')
const ctx = canvas.getContext('2d')
const socket = io()

// moveNet || blazePose
const MODEL = 'moveNet'
const parts = ['nose', 'left_wrist', 'right_wrist']
let currentStream
let detector

const debounce = (fn, delay) => {
  var timer = null
  return function () {
    var context = this, args = arguments
    clearTimeout(timer)
    timer = setTimeout(function () {
      fn.apply(context, args)
    }, delay)
  }
}

const stop = stream => stream.getTracks().forEach(tr => tr.stop())

// list available cams on select tag
const getCams = async _ => {
  const devices = await navigator.mediaDevices.enumerateDevices()
  return devices.filter(dev => dev.kind === "videoinput")
}

// load selected option cam
const loadCam = async cam =>{
  if(typeof currentStream !== 'undefined'){
    stop(currentStream)
  }

  const videoConstrains = {
    deviceId: {exact: cam.value},
    width: 640,
    height: 480,
  }

  if (navigator.mediaDevices.getUserMedia) {
    try {
      const stream = await navigator.mediaDevices
        .getUserMedia({video:videoConstrains, audio:false})
      video.srcObject = stream;
      video.play()
    }catch(e){
      console.log("Something went wrong!");
      console.log(e)
    }

  }
}

// load model and detector and start detecting
const loadModel = async () => {
  // config for blazePose
  let detectorConfig
  if(MODEL === 'blazePose'){
    detectorConfig = {
      runtime: 'tfjs',
      enableSmoothing: true,
      modelType: 'full'
    }
  }else if(MODEL === 'moveNet'){
    // config for moveNet 
    detectorConfig = {
      modelType: poseDetection.movenet.modelType.MULTIPOSE_LIGHTNING,
      enableTracking: true,
      trackerType: poseDetection.TrackerType.BoundingBox
    }
  }
  let model = MODEL === 'moveNet' 
    ? poseDetection.SupportedModels.MoveNet 
    : poseDetection.SupportedModels.BlazePose

  detector = await poseDetection.createDetector(model, detectorConfig);
  detectPoses()
}

const sendPose = (poses,idx) =>{
  for (let i = 0 ; i < poses[idx].keypoints.length; i++){
    for(let j = 0; j < parts.length; j ++){
      //console.log('parts', parts[j])
      //console.log('poses', poses[0].keypoints[i])
      if(parts[j] === poses[idx].keypoints[i].name){
        const data = poses[idx].keypoints[i]
        const dataStr = `pose_${idx}_${data.name}`
        //console.log('sending', dataStr)
        //console.log(data)
        //draw
        ctx.fillStyle = "#FF00FF";
        ctx.fillRect(data.x,data.y,10,10);
        //send
        //debounce(()=> socket.emit(dataStr, data),50)()
        socket.emit(dataStr,data)
      }
    }
  }
}

// loop to detect poses
const detectPoses = async ()=>{

  //pose estimation
  let poses;
  if(MODEL === 'blazePose'){
    const estimationConfig = {flipHorizontal:true}
    const timestamp = performance.now()
    poses = await detector.estimatePoses(video, estimationConfig, timestamp)
  }else if(MODEL === 'moveNet'){
    poses = await detector.estimatePoses(video)
  }

  ctx.clearRect(0, 0, canvas.width, canvas.height);


  if(poses[0]){
    sendPose(poses,0)
  }

  if(poses[1]){
    sendPose(poses,1)
  }

  if(EAGER){
    setTimeout(_ => detectPoses(), 5)//cap at N ms per frame
  }else{
    requestAnimationFrame(detectPoses)
  }
}


async function main(){
  //video
  const cams = await getCams()
  loadCam(cams[0])
  video.addEventListener('loadeddata', loadModel)
}
main()
